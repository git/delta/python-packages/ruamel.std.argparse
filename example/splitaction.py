from __future__ import print_function

from ruamel.std.argparse import SplitAppendAction
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-d', action=SplitAppendAction)

print(parser.parse_args("-d ab -d cd -d kl -d mn".split()))
print(parser.parse_args("-d ab,cd,kl,mn".split()))
print(parser.parse_args("-d ab,cd -d kl,mn".split()))
