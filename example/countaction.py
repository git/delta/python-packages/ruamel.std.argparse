from __future__ import print_function

from ruamel.std.argparse import CountAction
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--verbose', '-v', action=CountAction, const=1, nargs=0)
parser.add_argument('--quiet', '-q', action=CountAction, dest='verbose',
                    const=-1, nargs=0)

print(parser.parse_args("--verbose -v -q".split()))
