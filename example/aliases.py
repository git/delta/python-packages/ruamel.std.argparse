from __future__ import print_function

import sys
from ruamel.std.argparse import ArgumentParser, SubParsersAction

parser = ArgumentParser()
if sys.version_info < (3,):  # add aliases support
    parser.register('action', 'parsers', SubParsersAction)
subparsers = parser.add_subparsers()
checkout = subparsers.add_parser('checkout', aliases=['co'])
checkout.add_argument('foo')
args = parser.parse_args(['co', 'bar'])
print(args)
