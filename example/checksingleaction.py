from __future__ import print_function

from ruamel.std.argparse import CheckSingleStoreAction
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--check', '-c', action=CheckSingleStoreAction, const=1,
                    nargs=0)

print(parser.parse_args("--check -c".split()))
