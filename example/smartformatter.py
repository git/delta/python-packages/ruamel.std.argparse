from __future__ import print_function

from ruamel.std.argparse import SmartFormatter
import argparse


def exit(self, *args, **kw):
    pass


argparse.ArgumentParser.exit = exit

# the 'D|....' in the second pass triggers generating defaults for all entries,
# while being smart about which one already have a %(default)s

for index, log_s in enumerate(['log to file', 'D|log to file']):
    parser = argparse.ArgumentParser(formatter_class=SmartFormatter)

    parser.add_argument('--log', default='abc.log', help=log_s)
    parser.add_argument('--username',
                        help='username to login with (default: %(default)s)')
    parser.add_argument('--password', help='*|password to use for login')
    parser.add_argument('--recursive', '-r', action='store_true',
                        help="R|recurse into subdirectories \nto find files")
    parser.set_defaults(username='anthon', password="test123")

    if index > 0:
        print('--------------------------------------\n')
    parser.parse_args(["--help"])
